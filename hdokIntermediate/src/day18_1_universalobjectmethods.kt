class Client1(val name: String, val postalCode: Int)

class Client2(val name: String, val postalCode: Int) {
    override fun toString(): String = "Client2(name=$name, postalCode=$postalCode"
}

class Client3(val name: String, val postalCode: Int) {
    override fun toString(): String = "Client3(name=$name, postalCode=$postalCode"

    override fun equals(other: Any?): Boolean {
        if (other == null ||other !is Client3)
            return false
        return name == other.name && postalCode == other.postalCode
    }
}

class Client4(val name: String, val postalCode: Int) {
    override fun toString(): String = "Client4(name=$name, postalCode=$postalCode"

    override fun equals(other: Any?): Boolean {
        if (other == null ||other !is Client4)
            return false
        return name == other.name && postalCode == other.postalCode
    }

    override fun hashCode(): Int = name.hashCode() * 31 + postalCode
}

data class Client5(val name: String, val postalCode: Int)


fun main(args: Array<String>) {
    val c1 = Client1("Martin Lösch", 76698)
    println("Client1: $c1")

    println("----------------------------------------------------")

    val c2_1 = Client2("Martin Lösch", 76698)
    val c2_2 = Client2("Martin Lösch", 76698)
    println("Client2: $c2_1")
    println("Client2 equality: ${c2_1 == c2_2}")

    println("----------------------------------------------------")

    val c3_1 = Client3("Martin Lösch", 76698)
    val c3_2 = Client3("Martin Lösch", 76698)
    println("Client3 equality: ${c3_1 == c3_2}")
    println("Client3 identity with '===': ${c3_1 === c3_2}")
    val processed1 = hashSetOf(c3_1)
    println("c3_2 in set? -> ${processed1.contains(c3_2)}")

    println("----------------------------------------------------")

    val c4_1 = Client4("Martin Lösch", 76698)
    val c4_2 = Client4("Martin Lösch", 76698)
    println("'==': ${c4_1 == c4_2}")
    println("'===': ${c4_1 === c4_2}")
    println("Hashcodes: ${c4_1.hashCode()} <-> ${c4_2.hashCode()}")
    val processed2= hashSetOf(c4_1)
    println("Set: $processed2")
    println("c4_2 in set? -> ${processed2.contains(c4_2)}")
    processed2.add(c4_2)
    println("Set: $processed2")

    println("----------------------------------------------------")

    val c5_1 = Client5("Martin Lösch", 76698)
    val c5_2 = Client5("Martin Lösch", 76698)
    println("$c5_1, $c5_2")
    println("'==': ${c5_1 == c5_2}")
    println("'===': ${c5_1 === c5_2}")
    println("Hashcodes: ${c5_1.hashCode()} <-> ${c5_2.hashCode()}")
    val processed3= hashSetOf(c5_1)
    println("Set: $processed3")
    println("c5_2 in set? -> ${processed3.contains(c5_2)}")
    processed3.add(c5_2)
    println("Set: $processed3")
}
