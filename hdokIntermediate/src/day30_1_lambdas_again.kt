
val numbers = mapOf(0 to "zero", 1 to "one")
val people = listOf(Pson("Alice", 31), Pson("Bob", 29), Pson("Carol", 31))
val list1 = listOf("a", "ab", "b")
val list2 = listOf("abc", "def", "ghij")
val list3 = listOf(listOf(1,3,14), listOf(1,534,32,4), listOf(1, 54, 34))

fun main(args: Array<String>){
    println(numbers.mapValues { it.value.toUpperCase() })

    println(people.groupBy { it.age })
    println(list1.groupBy(String::first))

    println(list2.flatMap { it.toList() })
    println(list3.flatten())
}



