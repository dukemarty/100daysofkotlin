
fun salute() = println("Salute!")

data class PersonD22(val name: String, val age: Int)

fun PersonD22.isAdult() = age >= 18

fun main(args: Array<String>){
    run(::salute)

    println("--------------------------------------")

    val createPerson = ::PersonD22
    val p = createPerson("Bob", 22)
    println(p)

    println("--------------------------------------")

    val predicate = PersonD22::isAdult
    println("Is Bob adult? ${predicate(p)}")

    println("--------------------------------------")

    val bobsAgeFunction = p::age
    val bobsAdultFunction = p::isAdult
    println(bobsAgeFunction())
    println(bobsAdultFunction())
}