data class Person2(val name: String, val age: Int)

fun main(args: Array<String>) {
    val people = listOf(Person2("Bob", 29), Person2("Alice", 31), Person2("Caesar", 83),
            Person2("Zacharias", 7), Person2("Michael", 16))
    println("Oldest: ${people.maxBy { it.age }}")
    println("Oldest: ${people.maxBy(Person2::age)}")
    println("Youngest: ${people.minBy { it.age }}")
    println("Youngest: ${people.minBy(Person2::age)}")

    println("----------------------------------------------")

    val fx = { x: Int -> 3 * x * x + 2 * x + 7 }
    println("f(5) = ${fx(5)}")

    run { println(42) }
}
