
data class Client6(val name: String, val postalCode: Int)


fun main(args: Array<String>) {
    println("----------------------------------------------------")

    val c6_1 = Client6("Martin Lösch", 76698)
    val c6_2 = Client6("Martin Lösch", 76698)
    println("$c6_1, $c6_2")
    println("'==': ${c6_1 == c6_2}")
    println("'===': ${c6_1 === c6_2}")
    println("Hashcodes: ${c6_1.hashCode()} <-> ${c6_2.hashCode()}")
    val processed= hashSetOf(c6_1)
    println("Set: $processed")
    println("c6_2 in set? -> ${processed.contains(c6_2)}")
    processed.add(c6_2)
    println("Set: $processed")

    val copy1 = c6_1.copy()
    val copy2 = c6_1.copy("Uta Lösch")
    val copy3 = c6_1.copy(postalCode = 76600)
    val copy4 = c6_1.copy("Uta Lösch", 76600)
    println("$copy1")
    println("$copy2")
    println("$copy3")
    println("$copy4")
}
