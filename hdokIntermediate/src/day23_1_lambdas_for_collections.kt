data class Pson(val name: String, val age: Int)

fun main(args: Array<String>) {
    val list1 = listOf(1, 2, 3, 4)
    val list2 = listOf(Pson("Alice", 29), Pson("Bob", 31))

    println(list1.filter { it % 2  == 0})
    println(list2.filter { it.age > 30 })

    println("----------------------------------------")

    println(list1.map{ it*it})

    println("----------------------------------------")

    println(list2.map { it.name })
    println(list2.map( Pson::name ))

    println("----------------------------------------")

    println(list2.filter{ it.age > 30 }.map(Pson::name))
}
