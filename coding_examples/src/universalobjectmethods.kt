class ClientManual(val name: String, val postalCode: Int) {
    override fun toString(): String = "Client4(name=$name, postalCode=$postalCode"

    override fun equals(other: Any?): Boolean {
        if (other == null ||other !is ClientManual)
            return false
        return name == other.name && postalCode == other.postalCode
    }

    override fun hashCode(): Int = name.hashCode() * 31 + postalCode

    fun copy(name: String = this.name, postalCode: Int = this.postalCode) =
            ClientManual(name, postalCode)
}

data class ClientDataclass(val name: String, val postalCode: Int)

