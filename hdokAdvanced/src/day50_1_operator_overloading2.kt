import java.time.LocalDate

operator fun Point.get(index: Int): Int {
    return when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException("Invalid coordinate $index")
    }
}


data class MutablePoint(var x: Int, var y: Int)

operator fun MutablePoint.set(index: Int, value: Int) {
    when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException("Invalid coordinate $index")
    }
}


data class Rectangle(val upperLeft: Point, val lowerRight: Point)

operator fun Rectangle.contains(p: Point): Boolean {
    return p.x in upperLeft.x until lowerRight.x &&
            p.y in upperLeft.y until lowerRight.y
}


operator fun ClosedRange<LocalDate>.iterator(): Iterator<LocalDate> =
        object : Iterator<LocalDate> {
            var current = start

            override fun hasNext(): Boolean = current <= endInclusive

            override fun next(): LocalDate = current.apply {
                current = plusDays(1)
            }
        }

fun main(args: Array<String>) {
    val p1 = Point(10, 20)
    println(p1[1])

    println("----------------------------------")

    val p2 = MutablePoint(10, 20)
    println("p2 before setting: $p2")
    p2[1] = 42
    println("p2 after setting: $p2")

    println("==================================")

    val rect1 = Rectangle(Point(10, 20), Point(50, 50))
    println(Point(20, 30) in rect1)
    println(Point(5, 5) in rect1)

    println("==================================")

    val now = LocalDate.now()
    val vacation = now..now.plusDays(10)
    println(now.plusWeeks(1) in vacation)

    val newYear = LocalDate.ofYearDay(2017, 1)
    val daysOff = newYear.minusDays(1)..newYear
    for (dayOff in daysOff) {
        println(dayOff)
    }
}
