data class Point2(val x: Int, val y: Int) {
    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is Point) return false
        return other.x == x && other.y == y
    }
}

operator fun Point2.plus(other: Point2): Point2 {
    return Point2(x + other.x, y + other.y)
}

operator fun Point2.times(scale: Double): Point2 {
    return Point2((x * scale).toInt(), (y * scale).toInt())
}


fun main(args: Array<String>) {
    val p1 = Point2(10, 20)
    val p2 = Point2(30, 40)

    println("p1 + p2 = ${p1 + p2}")
    println("p1 * 1.5 = ${p1 * 1.5}")
}