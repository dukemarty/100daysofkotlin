import java.beans.PropertyChangeListener
import java.beans.PropertyChangeSupport
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

open class PropertyChangeAware {
    protected val changeSupport = PropertyChangeSupport(this)

    fun addPropertyChangeListener(listener: PropertyChangeListener){
        changeSupport.addPropertyChangeListener(listener)
    }

    fun removePropertyChangeListener(listener: PropertyChangeListener){
        changeSupport.removePropertyChangeListener(listener)
    }
}


// ========================================================================================
class Person1Manually(val names: String, age: Int, salary: Int): PropertyChangeAware() {

    var age: Int = age
        set(newValue) {
            val oldValue = field
            field = newValue
            changeSupport.firePropertyChange("age", oldValue, newValue)
        }

    var salary: Int = salary
        set(newValue) {
            val oldValue = field
            field = newValue
            changeSupport.firePropertyChange("salary", oldValue, newValue)
        }
}


// ========================================================================================
class ObservableProperty1(
        val propName: String, var propValue: Int,
        val changeSupport: PropertyChangeSupport
) {
    fun getValue(): Int = propValue
    fun setValue(newValue: Int) {
        val oldValue = propValue
        propValue = newValue
        changeSupport.firePropertyChange(propName, oldValue, newValue)
    }
}

class Person2WithObservable1(
        val name: String, age: Int, salary: Int): PropertyChangeAware() {

    val _age = ObservableProperty1("age", age, changeSupport)
    var age: Int
        get() = _age.getValue()
        set(value) { _age.setValue(value)}

    val _salary = ObservableProperty1("salary", salary, changeSupport)
    var salary: Int
        get() = _salary.getValue()
        set(value) { _salary.setValue(value)}
}


// ========================================================================================
class ObservableProperty2(
        var propValue: Int, val changeSupport: PropertyChangeSupport
) {
    operator fun getValue(p: Person3Operatored, prop: KProperty<*>): Int = propValue

    operator fun setValue(p: Person3Operatored, prop: KProperty<*>, newValue: Int) {
        val oldValue = propValue
        propValue = newValue
        changeSupport.firePropertyChange(prop.name, oldValue, newValue)
    }
}

class Person3Operatored(
        val name: String, age: Int, salary: Int): PropertyChangeAware() {

    var age: Int by ObservableProperty2(age, changeSupport)
    var salary: Int by ObservableProperty2(salary, changeSupport)
}



// ========================================================================================
class Person4WithStdDelegate(
        val name: String, age: Int, salary: Int
): PropertyChangeAware() {
    private val observer = {
        prop: KProperty<*>, oldValue: Int, newValue: Int ->
        changeSupport.firePropertyChange(prop.name, oldValue, newValue)
    }

    var age: Int by Delegates.observable(age, observer)
    var salary: Int by Delegates.observable(salary, observer)
}



fun main(args: Array<String>) {
    val p1 = Person1Manually("Dmitry", 34 , 2000)
    p1.addPropertyChangeListener(PropertyChangeListener { event ->
        println("Property ${event.propertyName} changed from ${event.oldValue} to ${event.newValue}")
    })
    p1.age = 35
    p1.salary = 2100

    println("==========================================================")

    val p2 = Person2WithObservable1("Dmitry", 34 , 2000)
    p2.addPropertyChangeListener(PropertyChangeListener { event ->
        println("Property ${event.propertyName} changed from ${event.oldValue} to ${event.newValue}")
    })
    p2.age = 35
    p2.salary = 2100

    println("==========================================================")

    val p3 = Person3Operatored("Dmitry", 34 , 2000)
    p3.addPropertyChangeListener(PropertyChangeListener { event ->
        println("Property ${event.propertyName} changed from ${event.oldValue} to ${event.newValue}")
    })
    p3.age = 35
    p3.salary = 2100

    println("==========================================================")

    val p4 = Person4WithStdDelegate("Dmitry", 34 , 2000)
    p4.addPropertyChangeListener(PropertyChangeListener { event ->
        println("Property ${event.propertyName} changed from ${event.oldValue} to ${event.newValue}")
    })
    p4.age = 35
    p4.salary = 2100
}
