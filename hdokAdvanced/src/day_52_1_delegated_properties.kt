import kotlin.reflect.KProperty

class DataBackend(var data: Int) {

    operator fun getValue(f: Foo, prop: KProperty<*>): Int {
        return data
    }

    operator fun setValue(f: Foo, prop: KProperty<*>, newValue: Int) {
        data = newValue
    }
}


class Foo(val loadSource: String) {
    var p: Int by DataBackend(13)
    val q by lazy { readQsFromSource() }
}

fun Foo.readQsFromSource(): Set<Int> {
    val res = setOf(13, 2, 12, 0, 1)
    println("Reading Qs from source (taking time): $loadSource")
    return res
}


fun main(args: Array<String>) {
    val f = Foo("http://blabla.foo.bar")

    println("Data p=${f.p}")
    f.p = 42
    println("Data p=${f.p}")
    println("${f.q}")

    println("================================")
}