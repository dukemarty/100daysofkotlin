fun String.filter(predicate: (Char) -> Boolean): String {
    val sb = StringBuilder()
    for (index in 0 until length) {
        val element = get(index)
        if (predicate(element)) sb.append(element)
    }
    return sb.toString()
}


fun <T> List<T>.filter(predicate: (T) -> Boolean): List<T> {
    val res = mutableListOf<T>()
    for (index in 0 until size) {
        val element = get(index)
        if (predicate(element)) res.add(element)
    }

    return res
}

fun main(args: Array<String>) {
    println("ab1c".filter { it in 'a'..'z' })
    println("ab1c".toList().filter { it in 'a'..'z' }.toString())
}