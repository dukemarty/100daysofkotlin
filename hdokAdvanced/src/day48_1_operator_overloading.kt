
// day 48 = 20th of august 2018, monday

data class Point(val x: Int, val y: Int) {
    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is Point) return false
        return other.x == x && other.y == y
    }
}

operator fun Point.plus(other: Point): Point {
    return Point(x + other.x, y + other.y)
}

operator fun Point.times(scale: Double): Point {
    return Point((x * scale).toInt(), (y * scale).toInt())
}

operator fun Point.unaryMinus(): Point {
    return Point(-x, -y)
}


operator fun Char.times(count: Int): String {
    return toString().repeat(count)
}


data class Person(val firstName: String, val lastName: String) : Comparable<Person> {
    override fun compareTo(other: Person): Int {
        return compareValuesBy(this, other, Person::lastName, Person::firstName)
    }
}


fun main(args: Array<String>) {
    val p1 = Point(10, 20)
    val p2 = Point(30, 40)
    var p3 = Point(5, 6)

    println("Base data: p1=$p1, p2=$p2")
    println("p1 + p2 = ${p1 + p2}")
    println("p1 * 1.5 = ${p1 * 1.5}")

    println("----------------------------")

    println('a' * 3)

    println("----------------------------")

    println("p3 before += p2: $p3")
    p3 += p2
    println("after += p2: $p3")
    println("-p1 = ${-p1}")

    println("=================================")

    println("(10/20)==(10/20): ${Point(10, 20) == Point(10, 20)}")
    println("(10/20)==(5/5): ${Point(10, 20) == Point(5, 5)}")
    @Suppress("SENSELESS_COMPARISON")
    println("null==(1/2): ${null == Point(1, 2)}")

    println("=================================")

    val m1 = Person("Alice", "Smith")
    val m2 = Person("Bob", "Johnson")
    println("$m1 < $m2 = ${m1 < m2}")
}