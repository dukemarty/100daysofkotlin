

val commands = mapOf(
        "HELP" to ::showHelp,
        "HELLO" to ::startClock,
        "BYE" to ::stopClock)

fun showHelp(args: Array<String>){
    if (args.size == 1){
        println("Showing general usage help")
    } else {
        println("Showing help for topic '${args[1]}'")
    }
}

fun startClock(args: Array<String>){
    if (args.size == 1){
        println("Starting clock without message")
    } else {
        println("Starting clock for task: '${args.drop(1).joinToString(separator = " ")}'")
    }
}

fun stopClock(args: Array<String>){
    if (args.size == 1){
        println("Stopping clock without message")
    } else {
        println("Stopping clock for task: '${args.drop(1).joinToString(separator = " ")}'")
    }
}

fun main(args: Array<String>){
    println("Arguments: ${args.joinToString()}")
    if (commands.containsKey(args[0])) {
        commands[args[0].toUpperCase()]?.invoke(args)
    } else {
        print("Unknown command '${args[0]}'")
    }
}

