import java.util.*
import java.io.*


val BRACKETS = setOf('(', ')', '[', ']', '{', '}')

fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    val expression = input.next()
    var e = expression.filter{ it in BRACKETS }

    var l = e.length + 1;

    while (e.length < l) {
        l = e.length
        for (s in listOf("()", "[]", "{}")){
            e = e.replace(s, "")
        }
    }

    println(e.length == 0)
}
