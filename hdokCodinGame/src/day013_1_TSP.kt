import java.util.*
import java.math.*
// import kotlin.math.*

class City(val x: Int, val y: Int)

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fun main(args : Array<String>) {

    fun dist(a: City, b: City): Double = Math.sqrt(Math.pow((a.x - b.x).toDouble(), 2.0) + Math.pow((a.y - b.y).toDouble(), 2.0))

    // fun dist(a: City, b: City): Double = sqrt((a.x - b.x).toDouble().pow(2) + (a.y - b.y).toDouble().pow(2))

    val input = Scanner(System.`in`)
    val N = input.nextInt()
    val unvisited = HashSet<City>()
    val start = City(input.nextInt(), input.nextInt())
    for (i in 1 until N) {
        val X = input.nextInt()
        val Y = input.nextInt()
        unvisited.add(City(X, Y))
    }

    var res = 0.0
    var current = start
    while (unvisited.size > 0){
        val next = unvisited.minBy { city -> dist(current, city) }
        if (next == null) break
        res += dist(current, next)
        current = next
        unvisited.remove(next)
    }
    res += dist(current, start)

    println(Math.round(res))
}

