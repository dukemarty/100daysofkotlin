import java.util.*
import java.io.*
import java.lang.*


fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    val n = input.nextInt() // the number of temperatures to analyse

    if (n == 0){
        println(0)
        return
    }

    var res = input.nextInt()
    for (i in 1 until n) {
        val t = input.nextInt() // a temperature expressed as an integer ranging from -273 to 5526

        when {
            Math.abs(t) < Math.abs(res) -> res = t
            Math.abs(t) == Math.abs(res) && t > 0 -> res = t
        }
    }

    println(res)
}
