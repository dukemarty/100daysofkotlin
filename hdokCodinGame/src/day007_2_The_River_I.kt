import java.util.*


fun Long.calc_char_sum(): Long{
    var res = 0L
    for (c in this.toString()){
        res += c.toString().toLong()
    }

    return res
}


fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    var r1 = input.nextLong()
    var r2 = input.nextLong()

    while (r1 != r2){
        if (r1 < r2){
            r1 += r1.calc_char_sum()
        } else {
            r2 += r2.calc_char_sum()
        }
    }

    println(r1)
}