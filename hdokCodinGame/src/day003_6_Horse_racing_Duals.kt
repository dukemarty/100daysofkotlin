import java.util.*
import java.io.*
import java.lang.*

fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    val N = input.nextInt()

    val horses: MutableList<Int> = mutableListOf()
    for (i in 0 until N) {
        horses.add(input.nextInt())
    }
    horses.sort()

    var best = Math.abs(horses[1] - horses[0])
    for (i in 2 until horses.size) {
        val diff = Math.abs(horses[i] - horses[i - 1])
        if (diff < best) {
            best = diff
        }
    }

    println(best)
}
