import java.util.*
import java.io.*
import java.math.*

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/
fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    val lightX = input.nextInt() // the X position of the light of power
    val lightY = input.nextInt() // the Y position of the light of power
    var tx = input.nextInt() // Thor's starting X position
    var ty = input.nextInt() // Thor's starting Y position

    // game loop
    while (true) {
        val remainingTurns = input.nextInt() // The remaining amount of turns Thor can move. Do not remove this line.

        when {
            ty < lightY -> {
                ty += 1
                print("S")
            }
            ty > lightY -> {
                ty -= 1
                print("N")
            }
        }

        when {
            tx < lightX -> {
                tx += 1
                print("E")
            }
            tx > lightX -> {
                tx -= 1
                print("W")
            }
        }

        println("")
    }
}
