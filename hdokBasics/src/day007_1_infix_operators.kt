

infix fun Int.myadd(rval: Int): Int {
    return this + rval
}

infix fun Array<Double>.scalar(rval: Array<Double>): Double{
    var res = 0.0
    for (i in 0 until  this.size){
        res += this[i] * rval[i]
    }

    return res
}


fun main(args: Array<String>){
    println("3 myadd 4 = ${3 myadd 4}")

    println("[1.0, 2.0] scalar [3.0, 4.0] = ${arrayOf(1.0, 2.0) scalar arrayOf(3.0, 4.0)}")
}
