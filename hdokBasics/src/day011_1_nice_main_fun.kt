import java.util.*

// because of the used java version (?!) not yet available
// import java.time.LocalDateTime


fun main(args: Array<String>){
    val startTime = Date()

    println("Running 'test for timing program' program")
    println("=========================================\n")

    val rand = Random()
    Thread.sleep(rand.nextInt(1500).toLong())

    val endTime = Date()
    println("Time ran for ${endTime.time - startTime.time}ms")
}
