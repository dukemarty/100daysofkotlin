
class User(val id: Int, val name: String, val address: String){
    override fun toString(): String = "User($id, $name, $address)"
}

fun User.validateBeforeSave() {

    fun validate(value: String, fieldName: String){
        if (value.isEmpty()){
            throw IllegalArgumentException (
                    "Can't save user $id: empty $fieldName.")
        }
    }

    validate(name, "Name")
    validate(address, "Addrees")
}


fun saveUser(user: User) {
    user.validateBeforeSave()

    // Save user to the database
    println("'Storing' user <$user> 'to database' ;-)")
}


fun main(args: Array<String>){
    println("Testing data validation using extension functions")
    println("=================================================\n")

    println("Testing: ")
    val user1 = User(13, "Martin", "Hauptstraße")
    saveUser(user1)
    val user2 = User(0, "", "Bar")
    try {
        saveUser(user2)
    }
    catch (ex: IllegalArgumentException){
        println("Caught exception when trying to 'store' user <$user2> 'to database'")
    }
}