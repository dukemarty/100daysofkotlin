

class User17(val name: String){
    var address: String = "unspecified"
        set(value: String) {
            println("""
                Address was changed for $name:
                "$field" -> "$value".""".trimIndent())
            field = value
        }
}

class LengthCounter {
    var counter: Int = 0
        private set

    fun addWord(word: String) {
        counter += word.length
    }
}


fun main(args: Array<String>){
    val user1 = User17("Alice")
    user1.address = "Elsenheimerstrasse 47, 80687 Muenchen"

    println("-----------------------------------------------")

    val lengthCounter = LengthCounter()
    lengthCounter.addWord("Hi!")
    println(lengthCounter.counter)
}