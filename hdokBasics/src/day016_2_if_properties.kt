
interface User16 {
    val nickname: String
}


// Primary constructor property
class PrivateUser(override val nickname: String) : User16


// Custom getter
class SubscribingUser(val email: String) : User16 {
    override val nickname: String
        get() = email.substringBefore('@')
}


fun getFacebookName(fbId: Int): String = ">>$fbId<<"

// Property intializer
class FacebookUser(val accountId: Int) : User16 {
    override val nickname = getFacebookName(accountId)
}


fun main(args: Array<String>){
    println(PrivateUser("test@kotlinglang.org").nickname)
    println(SubscribingUser("test@kotlinlang.org").nickname)
    println(FacebookUser(13).nickname)
}
