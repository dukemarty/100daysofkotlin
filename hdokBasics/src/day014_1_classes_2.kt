interface State

interface View {
    fun getCurrentStateNested(): State
    fun getCurrentStateInner(): State
    fun restoreState(state: State) {}
}

class Button(val value: Int) : View {
    override fun getCurrentStateNested(): State = NestedButtonState(this.value)
    override fun getCurrentStateInner(): State = InnerButtonState()

    override fun restoreState(state: State) {

    }

    class NestedButtonState(private val explicitOuterValue: Int) : State {
        override fun toString(): String = explicitOuterValue.toString()
    }

    inner class InnerButtonState : State {
        override fun toString(): String = this@Button.value.toString()
    }
}


fun main(args: Array<String>) {
    val b1 = Button(13)
    val b2 = Button(42)

    println("b1 states: ${b1.getCurrentStateInner()}, ${b1.getCurrentStateNested()} (inner/nested)")
    println("b2 states: ${b2.getCurrentStateInner()}, ${b2.getCurrentStateNested()} (inner/nested)")
}
