

fun main(args: Array<String>){
    val src1 = "ab =  cd   =   efg    = 13"
    println("Splitting '$src1' with different parameters ...")

    val p1 = "="
    val split1 = src1.split(p1)
    println("'$p1': $split1")

    val p2 = " = "
    val split2 = src1.split(p2)
    println("'$p2': $split2")

    val p3 = "\\s*=\\s*"
    val split3 = src1.split(p3.toRegex())
    println("'$p3: ' $split3")
}
