sealed class Expr {
    class Num(val Value: Int) : Expr()
    class Sum(val left: Expr, val right: Expr) : Expr()
    class Diff(val left: Expr, val right: Expr) : Expr()
    class Prod(val left: Expr, val right: Expr) : Expr()
    class Div(val left: Expr, val right: Expr) : Expr()
}

fun eval(e: Expr): Int =
        when (e) {
            is Expr.Num -> e.Value
            is Expr.Sum -> eval(e.left) + eval(e.right)
            is Expr.Diff -> eval(e.left) - eval(e.right)
            is Expr.Prod -> eval(e.left) * eval(e.right)
            is Expr.Div -> eval(e.left) / eval(e.right)
        }

fun eval2(e: Expr): Int =
        when (e) {
            is Expr.Num -> {
                println("num: ${e.Value}")
                e.Value
            }
            is Expr.Sum -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("sum: $left + $right")
                left + right
            }
            is Expr.Diff -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("diff: $left - $right")
                left - right
            }
            is Expr.Prod -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("prod: $left * $right")
                left * right
            }
            is Expr.Div -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("div: $left / $right")
                left / right
            }
        }


fun main(args: Array<String>) {
    println(eval(Expr.Sum(Expr.Sum(Expr.Num(1), Expr.Prod(Expr.Num(2), Expr.Diff(Expr.Num(6), Expr.Num(4)))), Expr.Num(4))))
    println(eval2(Expr.Sum(Expr.Sum(Expr.Num(1), Expr.Prod(Expr.Num(2), Expr.Diff(Expr.Num(6), Expr.Num(4)))), Expr.Num(4))))
}
