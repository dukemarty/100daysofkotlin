
import java.util.Arrays


fun printList1(list: List<Int>){
    println(list)
}

fun <T> joinToString1(
        collection: Collection<T>,
        separator: String,
        prefix: String,
        postfix: String
): String {
    val result = StringBuilder(prefix)

    for ((index, element) in collection.withIndex()) {
        if (index > 0) result.append(separator)
        result.append(element)
    }

    result.append(postfix)
    return result.toString()
}

fun printList2(list: List<Int>){
    println(joinToString1(list, "; ", "(", ")"))
}

fun <T> joinToString2(
        collection: Collection<T>,
        separator: String = ", ",
        prefix: String = "",
        postfix: String = ""
): String{
    val result = StringBuilder(prefix)

    for ((index, element) in collection.withIndex()) {
        if (index > 0) result.append(separator)
        result.append(element)
    }

    result.append(postfix)
    return result.toString()
}

fun printList3(list: List<Int>){
    println(joinToString2(list))
}

fun printList4(list: List<Int>){
    println(joinToString2(list, prefix = "# ", postfix = ";"))
}

fun main(args: Array<String>){
    val list1 = listOf(1, 2, 3)
    printList1(list1)
    printList2(list1)
    printList3(list1)
    printList4(list1)
}

