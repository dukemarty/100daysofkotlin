


open class UserBase(val nickname: String)

class User1(nickname: String) : UserBase(nickname)

class User2 constructor(_nickname: String){
    val nickname: String

    init {
        nickname = _nickname
    }
}

class User3(_nickname: String){
    val nickname = _nickname
}

class User4(val nickname: String,
            val isSubscribed: Boolean = true){

    override fun toString(): String = "$nickname -> $isSubscribed"
}


fun main(args: Array<String>){
    val alice1 = User1("Alice1")
    val alice2 = User2("Alice2")
    val alice3 = User3("Alice3")
    val alice4 = User4("Alice4")
    println("${alice1.nickname} | ${alice2.nickname} | ${alice3.nickname} | ${alice4.nickname} -> ${alice4.isSubscribed}")

    val bob = User4("Bob", false)
    val carol = User4("Carol", isSubscribed = false)

    print("$alice4 | $bob | $carol")
}

