
interface Expr

class Num(val Value: Int) : Expr
class Sum(val left: Expr, val right: Expr) : Expr
class Diff(val left: Expr, val right: Expr) : Expr
class Prod(val left: Expr, val right: Expr) : Expr
class Div(val left: Expr, val right: Expr) : Expr

fun eval(e: Expr): Int =
        when (e){
            is Num -> e.Value
            is Sum -> eval(e.left) + eval(e.right)
            is Diff -> eval(e.left) - eval(e.right)
            is Prod -> eval(e.left) * eval(e.right)
            is Div -> eval(e.left) / eval(e.right)
            else -> throw IllegalArgumentException("Unknown expression")
        }

fun eval2(e: Expr): Int =
        when (e){
            is Num -> {
                println("num: ${e.Value}")
                e.Value
            }
            is Sum -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("sum: $left + $right")
                left + right
            }
            is Diff -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("diff: $left - $right")
                left - right
            }
            is Prod -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("prod: $left * $right")
                left * right
            }
            is Div -> {
                val left = eval2(e.left)
                val right = eval2(e.right)
                println("div: $left / $right")
                left / right
            }
            else -> throw IllegalArgumentException("Unknown expression")
        }


fun main(args: Array<String>){
    println(eval(Sum(Sum(Num(1), Prod(Num(2), Diff(Num(6), Num(4)))), Num(4))))
    println(eval2(Sum(Sum(Num(1), Prod(Num(2), Diff(Num(6), Num(4)))), Num(4))))
}
