import java.util.*
import java.util.Random

fun main(args: Array<String>){
    val input = Scanner(System.`in`)
    val rand = Random()
    val numberToGuess = rand.nextInt(1000) + 1

    println("I'm thinking of a number between 1 and 1000. Guess it!")
    var notFound = true
    do {
        val guess = input.next().toInt()
        when  {
            guess > numberToGuess -> println("Too big!")
            guess < numberToGuess -> println("Too small!")
            else -> {
                println("Correct!")
                notFound = false
            }
        }
    } while (notFound)
}
