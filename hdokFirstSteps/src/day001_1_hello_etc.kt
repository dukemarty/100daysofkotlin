
fun main(args: Array<String>){
    println("Hello, ${if (args.isNotEmpty()) args[0] else "world"}!")

    print("Max: ")
    println(max(13,42))

    val myMin = min(13,42)
    println("Min: $myMin")

    val myAvg = avg(13, 42)
    println("Avg: $myAvg")
}

fun max(a: Int, b: Int): Int {
    return if (a > b) a else b
}

fun min(a: Int, b: Int):Int = if (a < b) a else b

fun avg(a: Int, b: Int) = (a + b) / 2.0
