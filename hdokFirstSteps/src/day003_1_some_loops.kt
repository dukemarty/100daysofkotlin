

fun FizzBuzz(upTo: Int){
    for (i in 1..upTo){
        when {
            i % 15 == 0 -> println("FizzBuzz")
            i % 5 == 0 -> println("Buzz")
            i % 3 == 0 -> println("Fizz")
            else -> println(i)
        }
    }
}

fun main(args: Array<String>){
    println("FizzBuzz with up to 100")
    println("-----------------------")
    FizzBuzz(100)
}
