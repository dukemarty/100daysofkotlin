import java.util.*

fun main(args: Array<String>){
    val input = Scanner(System.`in`)

    println("thinking of a number between 1 and 1000. I will guess it! Hint me with '<', '>', '='")
    var lower=1
    var upper=1000
    var notFound = true
    do {
        val guess = (lower + upper) / 2
        println("I guess: $guess")
        val res = input.next()
        when  (res) {
            "<" -> {
                println("Too big!")
                upper = guess
            }
            ">" -> {
                println("Too small!")
                lower = guess
            }
            else -> {
                println("Correct!")
                notFound = false
            }
        }
    } while (notFound)
}
