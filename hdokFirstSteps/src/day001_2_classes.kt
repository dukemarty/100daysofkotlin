
import java.util.Random

class PersonV1(
        val Name: String,
        val isMarried: Boolean
)

class Rectangle(val height: Int, val width: Int){
    val isSquare: Boolean
        get(){
            return height == width
        }

    override fun toString(): String = "Rectangle($height, $width)"
}

fun createRandomRectangle(maxLen: Int): Rectangle{
    val random = Random()

    return Rectangle(random.nextInt(maxLen), random.nextInt(maxLen))
}


fun main(args: Array<String>){
    val p1 = PersonV1("Bob", true)
    println("Person 1: ${p1.Name}, ${p1.isMarried}")

    val rectangle1 = Rectangle(41,43)
    val rectangle2 = Rectangle(41,41)
    println("Rectangles squares?: ${rectangle1.isSquare} / ${rectangle2.isSquare}")

    println("Some random rectangles: ${createRandomRectangle(1000)}, ${createRandomRectangle(1000)}," +
            "${createRandomRectangle(1000)}")

    println("=====================================================")
    println("Trying to find a random square ;-)")
    while (true){
        val ra = createRandomRectangle(10)
        if (ra.isSquare){
            println("")
            println("Found one!!!: $ra")
            break
        }
        else{
            print(" $ra ")
        }
    }
}